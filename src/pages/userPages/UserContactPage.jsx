import UserNavbar from '../../components/userComponents/UserNavbar'
import Contact from '../../components/common/Contact/Contact'
import UserFooter from '../../components/userComponents/UserFooter';


const UserContactPage = () => {
  return(
    <>
    <UserNavbar/>
    <Contact/>
    <UserFooter/>
    </>
  )
};

export default UserContactPage;
