import React from 'react'
import UserNavbar from '../../components/userComponents/UserNavbar'
import Chat from '../../components/userComponents/Chat/Chat'

const ChatPage = () => {
  return (
    <>
    <UserNavbar/>
    <Chat/>
    </>
  )
}

export default ChatPage