import React from 'react'
import UserNavbar from '../../components/userComponents/UserNavbar'
import CheckOut from '../../components/userComponents/CheckOut'
import UserFooter from '../../components/userComponents/UserFooter'

const CheckOutPage = () => {
  return (
    <>
    <UserNavbar/>
    <CheckOut/>
    <UserFooter/>
    </>
  )
}

export default CheckOutPage