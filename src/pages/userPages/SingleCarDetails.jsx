import CarDetails from "../../components/userComponents/CarDetails"
import RatingList from "../../components/userComponents/RatingList"
import UserFooter from "../../components/userComponents/UserFooter"
import UserNavbar from "../../components/userComponents/UserNavbar"

const SingleCarDetails = () => {
  return (
    <>
    <UserNavbar/>
    <CarDetails/>
    <RatingList/>
    
    <UserFooter/>
    
    </>
  )
}

export default SingleCarDetails