
import UserNavbar from '../../components/userComponents/UserNavbar'
import UserFooter from '../../components/userComponents/UserFooter'
import BookingSucces from '../../components/userComponents/Booking/BookingSucces'


const BookingSuccessPage = () => {
  return (
    <>
    <UserNavbar/>
    <BookingSucces/>
    <UserFooter/>
    </>
  )
}

export default BookingSuccessPage