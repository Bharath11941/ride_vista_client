
import UserFooter from '../../components/userComponents/UserFooter'
import UserNavbar from '../../components/userComponents/UserNavbar'
import ProfileCard from '../../components/userComponents/ProfileCard'

const ProfilePage = () => {
  return (
    <>
    <UserNavbar/>
    <ProfileCard/>
    <UserFooter/>
    </>
  )
}

export default ProfilePage