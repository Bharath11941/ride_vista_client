import Contact from '../../components/common/Contact/Contact'
import PartnerFooter from '../../components/partnerComponents/PartnerFooter'
import PartnerNavbar from '../../components/partnerComponents/PartnerNavbar'

const PartnerContactPage = () => {
  return (
    <>
      <PartnerNavbar/>
      <Contact/>
      <PartnerFooter/>
    </>
  )
}

export default PartnerContactPage
