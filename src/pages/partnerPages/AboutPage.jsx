import About from "../../components/common/About/About";
import PartnerFooter from "../../components/partnerComponents/PartnerFooter";
import PartnerNavbar from "../../components/partnerComponents/PartnerNavbar";

const AboutPage = () => {
  return (
    <>
      <PartnerNavbar />
      <About />
      <PartnerFooter />
    </>
  );
};

export default AboutPage;
