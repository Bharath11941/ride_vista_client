import React from 'react'
import PartnerNavbar from '../../components/partnerComponents/PartnerNavbar'
import ProfileCard from '../../components/partnerComponents/ProfileCard'
import PartnerFooter from "../../components/partnerComponents/PartnerFooter";
const PartnerProfile = () => {
  return (
    <>
      <PartnerNavbar/>
      <ProfileCard/>
      <PartnerFooter/>
    </>
  )
}

export default PartnerProfile
