import React from "react";

const HomeCardShimmer = () => {
  return (
    <div className="flex flex-col flex-wrap md:ml-24 md:mr-16 md:flex-row gap-5 md:gap-16">
      <div className="flex-shrink-0 h-72 w-80 pb-3 bg-white border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700 rounded-md hover:bg-blue-100">
        <div className="animate-pulse mx-auto bg-gray-300 h-40 w-80 rounded" />
      </div>
      <div className="flex-shrink-0 h-72 w-80 pb-3 bg-white border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700 rounded-md hover:bg-blue-100">
        <div className="animate-pulse mx-auto bg-gray-300 h-40 w-80 rounded" />
      </div>
      <div className="flex-shrink-0 h-72 w-80 pb-3 bg-white border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700 rounded-md hover:bg-blue-100">
        <div className="animate-pulse mx-auto bg-gray-300 h-40 w-80 rounded" />
      </div>
      <div className="flex-shrink-0 h-72 w-80 pb-3 bg-white border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700 rounded-md hover:bg-blue-100">
        <div className="animate-pulse mx-auto bg-gray-300 h-40 w-80 rounded" />
      </div>
      <div className="flex-shrink-0 h-72 w-80 pb-3 bg-white border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700 rounded-md hover:bg-blue-100">
        <div className="animate-pulse mx-auto bg-gray-300 h-40 w-80 rounded" />
      </div>
      <div className="flex-shrink-0 h-72 w-80 pb-3 bg-white border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700 rounded-md hover:bg-blue-100">
        <div className="animate-pulse mx-auto bg-gray-300 h-40 w-80 rounded" />
      </div>
    </div>
  );
};

export default HomeCardShimmer;
